const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string' || str1.match(/\D/) !== null || str2.match(/\D/) !== null) {
    return false;
  }
  let res = [];
  if (str1.length < str2.length) {
    for (let i = 0; i < str1.length; i++) {
      res.push((Number(str1[i]) + Number(str2[i])).toString());
    }
    for (let i = str1.length; i < str2.length; i++) {
      res.push(str2[i]);
    }
  } else {
    for (let i = 0; i < str2.length; i++) {
      res.push((Number(str1[i]) + Number(str2[i])).toString());
    }
    for (let i = str2.length; i < str1.length; i++) {
      res.push(str1[i]);
    }
  }
  return res.join('');
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsCount = listOfPosts.filter(post => post.author === authorName).length;
  let commentsCount = 0;
  for (const post of listOfPosts) {
    if (post.comments !== undefined) {
      commentsCount += post.comments.filter(comment => comment.author === authorName).length;
    }
  }
  return 'Post:' + postsCount + ',comments:' + commentsCount;
};

const tickets = (people) => {
  let bank = { 100: 0, 50: 0, 25: 0 };
  for (const personBill of people) {
    let change = Number(personBill) - 25;
    while (change !== 0) {
      if (change - 100 >= 0 && bank[100] > 0) {
        change -= 100;
        bank[100]--;
      } else if (change - 50 >= 0 && bank[50] > 0) {
        change -= 50;
        bank[50]--;
      } else if (change - 25 >= 0 && bank[25] > 0) {
        change -= 25;
        bank[25]--;
      } else {
        break;
      }
    }
    if (change !== 0) {
      return 'NO';
    }
    bank[Number(personBill)]++;
  }
  return 'YES';
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
